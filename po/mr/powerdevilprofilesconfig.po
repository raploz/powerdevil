# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-06 02:53+0000\n"
"PO-Revision-Date: 2013-02-23 17:39+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: American English <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "चेतन खोना"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "chetan@kompkin.com"

#: EditPage.cpp:172
#, kde-format
msgid ""
"The KDE Power Management System will now generate a set of defaults based on "
"your computer's capabilities. This will also erase all existing "
"modifications you made. Are you sure you want to continue?"
msgstr ""

#: EditPage.cpp:176
#, kde-format
msgid "Restore Default Profiles"
msgstr "मूलभूत रूपरेषांचे पुन्हस्थापन करा"

#: EditPage.cpp:248
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: profileEditPage.ui:21
#, kde-format
msgid "On AC Power"
msgstr "AC वीजेवर"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: profileEditPage.ui:31
#, kde-format
msgid "On Battery"
msgstr "बॅटरी वीजेवर"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: profileEditPage.ui:41
#, kde-format
msgid "On Low Battery"
msgstr "कमी बॅटरी वीजेवर"

#~ msgid "Power Profiles Configuration"
#~ msgstr "वीज रूपरेषा संयोजना"

#~ msgid "A profile configurator for KDE Power Management System"
#~ msgstr "केडीई वीज व्यवस्थापन प्रणालीकरिता रूपरेषा संयोजक"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 दारिओ फ्रेड्डी"

#~ msgid "Dario Freddi"
#~ msgstr "दारिओ फ्रेड्डी"

#~ msgid "Maintainer"
#~ msgstr "पालक"
