# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2011, 2015, 2016, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-06 02:04+0000\n"
"PO-Revision-Date: 2020-10-04 17:00+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Martin Schlander"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mschlander@opensuse.org"

#: activitypage.cpp:59
#, kde-format
msgid ""
"The activity service is running with bare functionalities.\n"
"Names and icons of the activities might not be available."
msgstr ""
"Aktivitetstjenesten kører med basisfunktionalitet.\n"
"Navne og ikoner for aktiviteter vil måske ikke være tilgængelige."

#: activitypage.cpp:132
#, kde-format
msgid ""
"The activity service is not running.\n"
"It is necessary to have the activity manager running to configure activity-"
"specific power management behavior."
msgstr ""
"Aktivitetstjenesten kører ikke.\n"
"Det er påkrævet at aktivitetshåndteringen kører for at kunne indstille "
"aktivititetsspecifik opførsel for strømstyringen."

#: activitypage.cpp:227
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Strømstyringstjenesten lader til ikke at køre."

#. i18n: ectx: property (text), widget (QRadioButton, noSettingsRadio)
#: activityWidget.ui:19
#, kde-format
msgid "Do not use special settings"
msgstr "Brug ikke særlige indstillinger"

#. i18n: ectx: property (text), widget (QRadioButton, specialBehaviorRadio)
#: activityWidget.ui:29
#, kde-format
msgid "Define a special behavior"
msgstr "Bestem en særlig opførsel"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownScreenBox)
#: activityWidget.ui:41
#, kde-format
msgid "Never turn off the screen"
msgstr "Sluk aldrig for skærmen"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownPCBox)
#: activityWidget.ui:48
#, kde-format
msgid "Never shut down the computer or let it go to sleep"
msgstr "Luk aldrig computeren ned eller lad den slumre"

#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "Slumre"

#~ msgid "Hibernate"
#~ msgstr "Dvale"

#~ msgid "Shut down"
#~ msgstr "Luk ned"

#~ msgid "Always"
#~ msgstr "Altid"

#~ msgid "after"
#~ msgstr "efter"

#~ msgid " min"
#~ msgstr " min"

#~ msgid "PC running on AC power"
#~ msgstr "Pc'en kører på strømforsyning"

#~ msgid "PC running on battery power"
#~ msgstr "Pc'en kører på batteri"

#~ msgid "PC running on low battery"
#~ msgstr "Pc'en kører på lavt batteri"

#~ msgctxt "This is meant to be: Act like activity %1"
#~ msgid "Activity \"%1\""
#~ msgstr "Aktivitet \"%1\""

#~ msgid "Act like"
#~ msgstr "Optræd som"

#, fuzzy
#~| msgid "Use separate settings (advanced users only)"
#~ msgid "Use separate settings"
#~ msgstr "Brug separate indstillinger (kun for avancerede brugere)"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Strømstyringstjenesten lader til ikke at køre.\n"
#~ "Dette kan løses ved at starte eller skemalægge den i \"Opstart og "
#~ "nedlukning\""

#~ msgid "Suspend"
#~ msgstr "Suspendér"

#~ msgid "Never suspend or shutdown the computer"
#~ msgstr "Suspendér eller sluk aldrig computeren"

#~ msgid "Activities Power Management Configuration"
#~ msgstr "Konfiguration af strømstyring for aktiviteter"

#~ msgid "A per-activity configurator of KDE Power Management System"
#~ msgstr "Konfigurationsværktøj til KDE's strømstyringssystem pr. aktivitet"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 Dario Freddi"

#~ msgid ""
#~ "From this module, you can fine tune power management settings for each of "
#~ "your activities."
#~ msgstr ""
#~ "Fra dette modul kan du finjustere strømstyringssindstillinger for hver "
#~ "enkelt af dine aktiviteter."

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Vedligeholder"
